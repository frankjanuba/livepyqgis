# Geometry Park

[Live da Issue 19](https://gitlab.com/geocastbrasil/livepyqgis/issues/19)

Aqui vamos entender a criação de poligonos usando PyQgis.

É interessante, para quem está chegando agora, ver as lives anteriores, principalmente as duas primeiras lives ([Quebrando o gelo](../live_1/README.md), e [Kyle way of life](../live_1/README.md)). Pois elas já tratam de algumas coisas que vamos usar aqui.

Dividimos ela para não ficar muita coisa para uma live.  

Então primeiro vamos aprender como funciona a criação de vetores desde a forma mais (ponto, X, Y).

[Documentação](https://docs.qgis.org/3.4/en/docs/pyqgis_developer_cookbook/geometry.html)

## Compondo a Geometria - Teoria

### Ponto

Forma mais simples da geometria é o `ponto`.
Ele determina uma posição no espaço, não possuem área, volume ou comprimento.  
Na geografia, é composto pelo encontro de duas retas (X e Y, Latitude e Longititude, Norting e Easting).

![Ponto](http://4.bp.blogspot.com/-HIHE5H9e3Js/TmEueE5GnfI/AAAAAAAAAJQ/kDh5BijueK0/s1600/ponto+geometrico.jpg)

Através dela é possível derivar todas as formas geométicas.  

![Exemplos](https://www.infoescola.com/wp-content/uploads/2010/03/dados-geograficos-2.jpg)

### Linha

A segunda forma, mais simples da geometria é a `linha`.  
Ela pode ser definida como um conjunto de infinitos pontos, outros a definem como um ponto que se desloca, e de aocordo com a minha mãe, é um insumo essencial na profissão dela para ficar pedindo para os outros passarem no fundo da agulha.  
Já na geografia, a linha é composta por dois pontos (vértices) que determinam seu inicio e fim.
A partir dela, é possivel medir seu comprimento, não possue área e volume. E possuem [direção e sentido](https://mundoeducacao.bol.uol.com.br/fisica/direcao-sentido.htm).  

![Exemplos de Linhas](https://static.mundoeducacao.bol.uol.com.br/mundoeducacao/conteudo/vetores(1).jpg)

### Poligono

Vem do grego, junção de duas palavas "poli" = muitos e "gono" = ângulos.  

Alguns definem poligono com um conjunto de segumentos de retas onde o fim do último segmento de reta encontra com o inicio do primeiro segmento.
No poligono temos um sentido (horário ou anti-horário).  

![Exemplos de Poligonos](https://s3.static.brasilescola.uol.com.br/img/2018/09/poligonos-be.jpeg)

Nos poligonos já é possívem medir:

* Área;
* Perímetro;
* Volume (Quando esse possui altura);
* Largura e etc.

Na matemática, os poligonos podem ser simples ou complexos:

* Poligono simples: Quando seus seguimentos se tocam apenas nas extemidades e não se cruzam.
* Poligonos complexo: É quando á intercessão entre dois segumentos de uma mesma geometria.

![Poligono Complexo](https://static.todamateria.com.br/upload/po/li/poligonosimples.jpg)

Na geografia, o conceito de geometria simples é diferente, é quando uma feição é formada por apenas uma geometria, e não por várias, já um poligono complexo é considerado um `ERRO DE GEOMETRIA`.  

## E no código? Como é que fica?

No Qgis pontos, linhas e poligonos são representadas pela classe [`QgsGeometry`](https://qgis.org/pyqgis/3.4/core/QgsGeometry.html#qgis.core.QgsGeometry).  

No Qgis podemos representar as geometrias da seguinte forma:

* Pontos: [`QgsPoint()`](https://qgis.org/pyqgis/3.4/core/QgsPoint.html#qgis.core.QgsPoint) ou [`QgsPointXY()`](https://qgis.org/pyqgis/3.4/core/QgsPointXY.html#qgis.core.QgsPointXY). A diferença entre ambas as formas é que `QgsPoint` pode receber as dimensões M e Z;
* Linhas (linestring): São compostas por uma lista de pontos;
* Poligono: São compostos por 'anéis lineares' (tradução livre), onde o primeiro é a borda externa, e os outros, opcionais, são `buracos` internos. O qgis irá fechar as as linhas que compoem o poligono automaticamente, não sendo necessário repetir o primeiro ponto como ultimo.

[CoockBook PyQGIS](https://docs.qgis.org/testing/en/docs/pyqgis_developer_cookbook/geometry.html#id1)

Veremos mais tarde as multi geometrias.

### Direto ao Ponto

Todas as geometrias são baseadas no `QgsPointXY` ou `QgsPoint()`.

Ambos recebem 2 parâmetros para serem criados:

* `x` - Coordenada X, Longitude, Easting....
* `y` - Coordenada y, Latitude, Norting....

Tanto `x` quanto `y` devem ser numéricos e aceitam valores `reais`, `double` ou `inteiro`.

Exemplo:

```Python
ponto1 = QgsPoint(10,10)
ponto2 = QgsPoint(10.5, 10.5)
```

:warning: Em caso de coordenadas geográficas, o formato a ser informado é Graus Decimais.  
[Transformando de GMS para GD](http://forest-gis.com/2011/07/como-converter-graus-minutos-e-segundos-para-graus-decimais.html/)  
[Transformando de GD para GMS](https://www.stefanelli.eng.br/converter-grau-decimal-minuto-segundo/)

### Montando a Linha

Para se montar uma linha, podemos utilizar 2 modos de fazer.  
Usando o `QgsLineString(pontos)` podemos passar para ele uma lista de pontos que irá gerar a linha, seguindo a sequência de pontos da lista.

Exemplo:

```Python
geometria = QgsLinestring(pontos)
# OU
geometria = QgsGeometry.fromLineString([pontosXY])
```

Podemos utilizar também o método `QgsGeometry.fromLineString([pontos])`

### Montando o Poligono

Para montarmos uma geometria do tipo poligono, vamos precisar de usar `QgsPointXY()`.  
E temos de utilizar o metodo `QgsGeometry.fromPolygonXY([pontosXY])`.

Exemplo:

```Python
geometria = QgsGeometry.fromPolygonXY([pontosXY])
```
