# Live 2: Aprendendo no sufoco  
  
**[![Live pyqgis 2 - Aprendendo no sufoco](https://img.youtube.com/vi/eZHz4SXR8R4/0.jpg)](https://youtu.be/eZHz4SXR8R4)**  

## Revisão da live 1 e algumas alternativas  
Na [live passada](estudos/live_1)  executamos algumas atividades básicas usando dados do BCIM do IBGE. Nesta live, usaremos dados que foram organizados específicamente para a live e por isso, vocês terão que baixar [desta pasta do Google Drive](https://drive.google.com/drive/folders/1BIb1sCd2G4qPLqrEYapQx-5w_gX5l1lC?usp=sharing). Basta baixar a pasta `dados` e incluí-la tal como está no repositório `livepyqgis`.  
 
### Organização de `path`

Como vamos trabalhar com alguns arquivos que estarão na pasta de nesso repositório e, pelo fato de cada um ter pastas com diferentes nomes, será importante sabermos usar um módulo muito importante chamado [os module](https://docs.python.org/3/library/os.html).  
 Isso nos permitirá não só criar código mais legíveis, como também, simplificar os tutoriais, padronizando os caminhos a partir da paste deste repositório (`/livepyqgis/...`).  

**Como faremos isso?**  
Antes de tudo, teremos que importar esse módulo, adicionando ao nosso script:

```python
import os
```

E vamos criar um objeto chamado "diretorio", o qual terá o caminho à pasta onde se encontra este repositório: "livepyqgis":

```python
diretorio = "path_to_livepyqgis"
```

Tendo feito isso, seguiremos usando o módulo `os` para compor o caminho até os nosso dado e/ou projetos.

```python
# Caminho até o GPKG base_dados
base_dados = os.path.join(diretorio, "livepyqgis/dados/base_dados.gpkg")
```

Se você sabe alguma coisa de python, vc deve estar se perguntando: **"qual a diferença em usar o módulo `os` ou qualquer outra forma de concatenação de string?** Afinal é o que estamos fazendo..."  
A pergunta é boa, mas não é trivial. O módulo `os`nos dá várias outras funcionalidades além da concatenação de string.  

Vamos dar um exemplo. Já temos o caminho ao diretório desse reposiório "livepyqgis", como objeto `diretorio`. Vamos criar um novo objeto (só para não ficar muito grande nossos comandos) com a concatenação até o geopackage BCIM:

```python
bcim = os.path.join(diretorio, "livepyqgis", "dados", "bcim_2016_21_11_2018.gpkg")
```

Agora, suponhamos que queiramos salvar um outro dado, ou até mesmo o resultado de uma análise nessa mesma pasta. Mas sem ter que escrever o caminho inteiro. Poderemos fazer isso com o método `os.path.diname()`, e ele já nos dará o caminho ate'a pasta em questão:

```python
# Pegando o caminho da pasta do arquivo
caminho = os.path.dirname(base_dados)
print( caminho )
```

Outro elemento que nos pode ser muito útil é a possibilidade de testar se, de fato, esse arquivo existe, usando o [` os.path.isfile`](https://docs.python.org/2/library/os.path.html#os.path.isfile):

```python
if os.path.isfile(bcim): # Testa se o arquivo é válido
  print( "Arquivo Existente!" )
else:
  print( "Arquivo Inexistente!" )
```

Você deve estar se perguntando: **Mas para quê validar a existencia de um aquivo?**  
Muitas vezes, quando estamos desenvolvendo uma rotina usando programação, estamos supondo algumas coisas. Por exemplo: quando queremos criar um script para carregar todos os dados vetoriais de uma determinada pasta ao nosso projeto, estamos supondo que:  
* Há dados na pasta;
* Os dados são do tipo vetorial (ou matricial);  
* Os dados são válidos (ou melhor, não estão corrompidos);  

Mas nem sempre essas suposições são verdadeiras, e não sendo, resultarão em erros. Então este tipo de abordagem é uma forma de antecipar-se aos potenciais erros.  

### Abrindo um arquivo GEOPACKAGE  

Já vimos na [live_1](estudos/live_1/README.md#abrir-uma-camada-vatorial) como usar o [`addVectorLayer()`](https://qgis.org/pyqgis/master/gui/QgisInterface.html#qgis.gui.QgisInterface.addVectorLayer), que é um método do `QgisInterface`. Agora vamos ver como podemos fazer isso com o `addVectorLayer()`.  
* Com o [`QgsVectorLayer()`](https://qgis.org/pyqgis/master/core/QgsVectorLayer.html#qgis.core.QgsVectorLayer) podemos acessar um *provedor* de dados vetoriais. Não necessariamente o importa ao projeto, mas já nos permite fazer algumas coisas. Seu uso ficaria da seguinte forma: `QgsVectorLayer(path, baseName, providerLib, options)`;  

```Python
QgsVectorLayer("/livepyqgis/dados/bcim_2016_21_11_2018.gpkg", 'lim_unidade_federacao_a', 'ogr')
```

**Esse métdo não adiciona a camada ao projeto**, ele o acessa e transforma em um objeto "QgsVectorLayer". Isso é muito útil se quisermos saber se a camada é válida antes de adicionar ao projeto, por exemplo.  

```python
if QgsVectorLayer("/livepyqgis/dados/bcim_2016_21_11_2018.gpkg", 'lim_unidade_federacao_a', 'ogr').isValid():
  print( "Essa camada é válida!" )
else:
    print( "Deu ruim" )
```

Posteriormente, podemos, enfim, adicionálo ao nosso projeto. Para isso, precisaremos instanciar o projeto que estemos trabalhando com o [`QgsProject.instance()`](https://qgis.org/pyqgis/master/core/QgsProject.html?highlight=qgsproject%20instance#qgis.core.QgsProject.instance) já apresentado na seção ["Salvando o projeto" da live_1](estudos/live_1/README.md#salvando-o-projeto) e usnado o método [`addMapLayer( sub_vlayer )`](https://qgis.org/pyqgis/master/core/QgsProject.html?#qgis.core.QgsProject.addMapLayer), que possui como parametro `mapLayer` que se refere à camada a ser adicionada;  

```python
sub_vlayer = QgsVectorLayer("/livepyqgis/dados/bcim_2016_21_11_2018.gpkg", 'lim_unidade_federacao_a', 'ogr')
QgsProject.instance().addMapLayer( sub_vlayer )
```  

### Listando camadas do GEOPACKAGE  

FONTE: Adaptado do [Stack Exchange](https://gis.stackexchange.com/questions/307252/qgis-3-python-load-all-layer-from-geopackage)

O formato [geopackage](http://www.geopackage.org/) funciona básicamente como um banco de dados que pode ser levado de um lado para o outro, sem precisar instalar um servidor em uma máquina para isso, tendo como base o [SQLite](https://sqlite.org/index.html).

Para se adicionar diversos arquivos de um `.gpkg` temos duas formas de se fazer:

1. usando o método [`subLayers()`](https://qgis.org/pyqgis/master/core/QgsDataProvider.html?highlight=sublayers#qgis.core.QgsDataProvider.subLayers) do [`QgsVectorLayer().dataProvider()`](https://qgis.org/pyqgis/master/core/QgsDataProvider.html?#module-QgsDataProvider): 

Exemplo: **Adicionando todas as camadas de um GPKG usando subLayers()**
```python

arquivo = QgsVectorLayer( bcim , "", 'ogr' )

sublayers = arquivo.dataProvider().subLayers()

for subLayer in subLayers():
	# print( subLayer )
	nome = subLayer.split( "!!::!!" )[ 1 ]
	# print( nome )
	uri = "%S|layername=%s" % ( bcim , nome )
	# print( uri )
	sub_vlayer = QgsVectorLayer( uri , nome , "ogr" )
	# Adiciona a camada ao mapa
	QgsProject.instance().addMapLayer( sub_vlayer )
```
  
2. Usando a biblioteca [`ogr`](https://gdal.org/python/osgeo.ogr-module.html):  

Para usar as classes, métodos e funções do `OGR`, recisaremos importá-lo em nosso script:

```python
from osgeo import ogr
```
A biblioteca `OGR` da [`OSGeo`](https://www.osgeo.org) é utilizada para trabarlhar dados vetoriais.

Em seguida vamos criar uma variável para receber o endereço do .gpkg, isso ajuda por não precisar ficar digitando o caminho o tempo todo... bastando informar a variável que contem activeLayer

:warning: o caminho foi utilizado no LINUX, sendo que no windows e outros poderá ser diferente.

```python
# Caminho completo do arquivo
gpkgPath = "livepyqgis/dados/bcim_2016_21_11_2018.gpkg"
```

Em seguida vamos criar um 'cursor' para conectar no arquivo usando o OGR e conseguir pegar os dados lá dentro, usando o método [`ogr.Open()`](https://gdal.org/drivers/vector/gpkg.html).

```python
con = ogr.Open( base_dados )
```

Para listar os nomes das camadas, basta usar um `for` para mostrar os nomes das camadas.
Nesse momento o metodo  `.GetName()` é o que nos retorna os nomes das camadas:

```python
for i in con:
  print( i.GetName() )
```

Nosso código completo ficaria assim:

```python
from osgeo import ogr
# ( ... )
con = ogr.Open(base_dados)
for i in con:
  print( i.GetName() )
```

## Adicionando TODAS as camadas do GPKG ao projeto  

Lembra do `iface.addVectorLayer(vectorLayerPath, basename, providerKey )`, visto no [modulo anterior](estudos/live_1/README.md#Abrir_uma_camada_vatorial)? Vamos usar ele aqui:

```python
for camada in con:
  vectorLayerPath = gpkgPath + "|layername="+ camada.GetName()
  basename = camada.GetName()
  iface.addVectorLayer(vectorLayerPath, basename, 'ogr' )
```

Para facilitar ainda mais, podemos transformar os parâmetros informados no `addVectorLayer` em variáveis e passar apenas as variáves para o metodo, assim o código fica melhor de se ler:

```python
for camada in con:
	uri = "%s|layername=%s" % ( base_dados , camada.GetName() )
    camadaVetorial = QgsVectorLayer( uri, camada.GetName(), 'ogr' ) 
    QgsProject.instance().addMapLayer( camadaVetorial )  
```