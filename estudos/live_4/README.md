# Live 4: Catalog on the fly paraguaio  
  
**[![Live pyqgis 4 - Catalog on the fly paraguaio](https://img.youtube.com/vi/mWkzXifz5MU/0.jpg)](https://www.youtube.com/watch?v=mWkzXifz5MU)**  

[Live da Issue 7](https://gitlab.com/geocastbrasil/livepyqgis/issues/7)  

 Nesta live, usaremos dados que foram organizados específicamente para a live e por isso, vocês terão que baixar [desta pasta do Google Drive](https://drive.google.com/drive/folders/1BIb1sCd2G4qPLqrEYapQx-5w_gX5l1lC?usp=sharing). Basta baixar a pasta `dados` e incluí-la tal como está no repositório `livepyqgis`.  

## Do que se trata  
 
A idéia nasceu a partir do seguinte [tweet](https://twitter.com/klaskarlsson/status/1159514845127028738) de [Klas Karlson](www.twiter.com/clascarlson)

> 2'000 raster files to keep track on and load into #QGIS easily on demand?
>
>Use:
>gdaltindex -write_absolute_path index.shp *.tif
>
>Add some Python code to an Action for the layer, and click >in the map to load the raster!
>
>(I used -f GPKG and *.nc, but you get the point)
>
![image](https://pbs.twimg.com/media/EBdsmabWwAAUtT1?format=jpg&name=small)

No tweet acima, ele gera um arquivo com o poligono da área das imagens e seu endereço absoluto em um arquivo `.shp` (originalmente ele usou um geopackage no lugar do .shp, dá no mesmo.).

A intenção é ter um catálogo de imagens que, quando é clicado sobre, ele traz as imagens da área para o qgis automaticamente, sem termos de ficar buscando as imagens nas pastas.  
O [Luiz Motta](https://github.com/lmotta) desenvoleu para o Qgis 2 plugins muito úteis que nos ajudam a fazer esse manejo das imagens que utilizamos, um é o [Image Footprint](https://github.com/lmotta/imagefootprint_plugin) com ele é possivel vasculhar um diretorio em busca de imagens e gerar um catálogo delas, o detalhe é que é possivel criar um poligono com a área útil da imagem, evitando assim de pegarmos uma imagem onde a área que queremos caiu na parte com `no data`.  
O outro plugin é o [Catalog On The Fly](https://github.com/lmotta/catalog-on-the-fly) que verifica as camadas vetoriais dentro do projeto em busca de camadas passivas de serem um catálogo e adiciona as imagens ao projeto.  
Por isso "Paraguaiam Catalog on the fly"...  

> Aaaaaahhhhhhh, intenddiiiiiiii!!!

Vale muito a pena instalar esses plugins e vê-los funcionando.  

## Preparando o Projeto

Em um projeto novo, vamos adicionar a camada 'grade_landsat' que está dentro do arquivo 'exemplo_catalogo.gpkg'.  

Nessa camada temos vários campos que vão nos ajudar a organizar nossas imagens, no caso desse exemplo, da conselação Landsat 8.  

* `fid`: Campo identificador único da feição, sequencial e de auto incremento.  
* `path`: Órbita da imagem, necessário na hora de fazer a requisição de uma nova imagem no INPE/EARTH EXPLORER etc.  
* `row`: Ponto da imagem, necessário na hora de fazer a requisição de uma nova imagem no INPE/EARTH EXPLORER etc.  
* `path_row`: O conjunto formado pela órbita e ponto da imagem, separados por '_'  
* `caminho`: Nesse campo, coloquei o nome das pastas em relação ao caminho da pasta do repositório, separadas por virgula (explicarei mais tarde).  
* `composicao`: Tipo de composição da imagem, BS = Banda Simples, CN = Cor Natural..
* `arquivo`: Nome do arquivo dentro da pasta.  
* `banda`: Bandas que compoem a imagem (b1, b2, ndvi...) ou conjunto de bandas que a compoem.  
* `data_imagem`: Data do imageamento.  
* `cena`: É o nome da cena, formado pela constelação (L8), orbita, ponto, ano, dia do ano etc.  

Essa é uma organização bem básica, que pode ser alterada conforme necessidade, para melhor organizar os dados.  

# Qgis, Python.. AÇÃO

AS ações são elementos muito úteis dentro do Qgis, nos permite abrir arquivos fisicos, como PDFs, Documentos, imagens de satélite, páginas de internet, e por aí vai. Usando python, é possível chegar em lugares onde ninguém jamais foi.

Adicionar arquivo geopackage exemplo_catalogo a um projeto  

Botão direito sobre a `camada > propriedades > Ações`:  

1. Criar uma ação python:  
2. Descrição.  
3. Nome Curto.
4. Escopo da ação: Telas e feição.  
5. Adicionar código na caixa de texto.

## Codar é preciso, viver não é preciso

Vamos ao código.

```python
# -*- coding=utf-8 -*-

from os import path
from qgis.PyQt import QtWidgets # Para abrir uma janela de erro para o usuario.
diretorio = "path_to_livepyqgis"
```

Até aqui está tudo bem...

```python
caminho = str('[%caminho%]').split(', ')
full_path = path.join(diretorio, *caminho, '[%arquivo%]')
```

Aqui começamos uma brincadeira bem bacana, como estamos trabalhando dentro do Qgis, ele nos permite acessar os atributos de uma camada vetorial sem a necessidade de fazermos um código para isso.

![OMG](https://media.giphy.com/media/XNACwQFyfBN60/giphy.gif)

Isso é possivel fazendo a requisição do dado usando `[%campo desejado%]`.  
Podemos inserir o campo no nosso código:

* Escrevendo o nome do campos usando o padrão acima descrito;  
* Escolhendo o campo na lista abaixo da caixa de texto da ação e clicar em Inserir;  
* Montar uma expressão na calculadora de campo e inserir no código;  

![desmaio](https://media.giphy.com/media/5sfN51bhaxoVq/giphy.gif)  

Fazendo isso o Qgis "enxerta" o valor do campo no código, facilitando assim a vida da gente.

No caso do código acima, como precisamos do dado como um texto, por isso colocamos `'[%caminho%]'` com as aspas simples.

> Mas, pq fez isso?  

Simples.
Eu coloquei no campo "caminho" os nomes das pastas na ordem para se chegar na imagem, separada por vírgula. Usando o codigo `str('[%caminho%]').split(', ')` o python quebra o texto no delimitador indicado (', ') transformando ele em uma lista.  

Na [linha 8 do código](https://gitlab.com/geocastbrasil/livepyqgis/blob/master/estudos/live_4/script.py#L8) o metodo `path.join()` faz todo o trabalho de montar o caminho completo até chegar no arquivo, usando o separador de pastas de acordo com o S.O. onde ele estiver rodando.  

> Como assim, homi, explica isso direito!?  

Essa é uma pratica interessante, pois temos de pensar que o código nem sempre vai rodar em um mesmo sistema operacional, e nem todos utilizam o mesmo padrão.  

> temos de pensar que o código nem sempre vai rodar em um mesmo sistema operacional, e nem todos utilizam o mesmo padrão. (Kyle Felipe, 2019)  

Por exemplo:  

Windows usa separador de pastas a '\\' que é um "meta caractere" de escape (já usou o '\n' no qgis para gerar rótulos?), ou seja, em uma string, quando deparamos com a '\\' significa que o proximo caractere é na verdade um comando. No caso do `'\n'` significa que naquele ponto uma nova linha deve ser iniciada.  
E para montar caminhos no Windows devemos usar a barra dobrada nas strings de caminho de arquivo, pois ela vai "escapar" ela mesma e o sistema vai entender que é uma barra literal, e não um escape.  

Ou seja, a string fica assim `'C:\\pasta_1\\pasta_2\\..\\arquivo.ext'`.  
Já no linux, é utilizada a contra barra '/' para a separação de pastas.  
Dessa forma, a gente não precisa ficar preocupando em saber qual S.O. estamos trabalhando, o python vai usar o separador padrão e utilizar para montar o caminho.  

> E o `*caminho` que você usou?

Isso no Python chama-se desempacotar.  
Ele utiliza retira cada elemento da lista e os usa do lado de fora dela, assim ele vai montar o caminho completo até meu arquivo.

Exemplo:

```python
from os import path

pastas = ['pasta_1', 'pasta_2', 'pasta_3', 'pasta_5']

caminho = path.join('C:', *pastas, 'arquivo.txt')
>> C:\\pasta_1\\pasta_2\\pasta_3\\pasta_5\\arquivo.txt
```

![mind blowing](https://media.giphy.com/media/xT0xeJpnrWC4XWblEk/giphy.gif)  

É por isso que sempre avisamos no começo das lives:

> Lembrem-se de colocar na variável 'tal', o caminho até o repositório

Chega de gifs....
Seguindo...

```python
# Verificando se o arquivo existe na pasta, antes de adicionar ao QGIS
if path.isfile(full_path):
    rasterLayer = QgsRasterLayer(full_path, '[%cena%]' )
    # Pegando o provedor de dados para configurar a simbologia, transparência, etc.
    provider = rasterLayer.dataProvider()
    if '[%composicao%]'== 'BS': # Verificando se o campo "composicao" da feição é 'BS'
        provider.setNoDataValue(1, 0) # Configurando o valor NO DATA da camada para 0
    QgsProject.instance().addMapLayer(rasterLayer)
```

O metodo `isfile()` do `path` verifica se o caminho existe.    
Caso seja válido, ele segue o código até adicionar a camada ao qgis.  
Caso contrário, ele abre uma janela de erro, informando que não há raster para aquela região informada.  

Não vamos aprofundar em Widgets agora, por ser um assunto mais complexo.    
É necessário entender da biblioteca [`Qt5`](https://www.riverbankcomputing.com/static/Docs/PyQt5/) que o qgis usa para montar a `GUI - Grafical User Interface`.  

# Ao infinto e além  
Eu já deixei uma imagem com a composição colorida (cor natural) já disponível para usarmos.  

Com um pouco mais de estudo, podemos montar formulários de cadastro, ou um script que verifica a presença de imagens em um determinado diretório e adiciona os dados à grade de imagens.  
Ler os metadados, e utilizá-los no processamento, organização.  
Já pensou em uma ação que, quando não encontra uma composição natural, montaria uma e já a adicionava o dado na tabela de atributos?  
Ou faria a requisição da imagem no site do INPE/EARTH EXPLORER?  
Gera-se o NDVI, ou qualquer outro indice.  

Acaba que nem o céu se torna uma limitação quando aplicamos programação como ferramenta de trabalho.  

![Unlimited Power](https://media.giphy.com/media/t7FhK9hwHlUqc/giphy.gif)

OBS:  
Composição das bandas L08: [http://www.processamentodigital.com.br/2013/11/23/qgis-2-0-composicao-colorida-rgb-para-imagens-landsat-8/](http://www.processamentodigital.com.br/2013/11/23/qgis-2-0-composicao-colorida-rgb-para-imagens-landsat-8/)  
