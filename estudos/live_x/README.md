# Live X: PyQgis Também Salva

Nós já aprendemos a abrir dados ( Lives 1, 1b, 2 e 3)
Aprendemos criar polígonos (Live 6 e 7).
Mas e se depois que aplicarmos todos esses procedimentos, quisermos persistir nossos dados??
É por isso que vamos aprender a salvar nossos dados em arquivos.
E vamos salvar nossos arquivos em [Geopackage](http://www.geopackage.org/) e também em formato [tiff](https://pt.wikipedia.org/wiki/Tagged_Image_File_Format)

## Classe QgsVectorFileWriter

Essa [classe](https://qgis.org/pyqgis/3.4/core/QgsVectorFileWriter.html) é utilizada para escrevermos vetores em arquivos no disco, sendo o método mais importante, agora, o [writAsVectorFoprmat()](https://qgis.org/pyqgis/3.4/core/QgsVectorFileWriter.html?highlight=qgsvectorfilewriter#qgis.core.QgsVectorFileWriter.writeAsVectorFormat) que é o método que escreve os dados no arquivo.

Esse método permite:

- Criar um novo arquivo com as feições.

- Adicionar feições à um arquivo já existente, substituindo a camada, caso o arquivo/camada com o mesmo nome já exista, essa sem acrescentar campos diferentes que possam existir no arquivo.

- Adicionar as feições a um arquivo já existente, adicionando os dados em uma camada, caso o arquivo/camada com o mesmo nome já exista, e acrescenta novo campos.

- Adicionar as feições à um arquivo já existente em camadas/tabelas diferentes (como geopackage, spatialite...).

## From Scratch

Lembra da live [Geometry Park](https://youtu.be/GlbIf8ZxpSE) e da live de [Gestão de Atributos](https://youtu.be/BaQvCjvl23Y)?
Vamos brincar um pouco com elas para para termos alguns arquivos para salvar.