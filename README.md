# Live de PyQGIS

Repositorio criado para iniciar os estudos da API [Python](https:\\www.python.org) do [QGIS](https:\\www.qgis.org) - [QGIS Python API - A.K.A.: PyQGIS](https://qgis.org/pyqgis/master/)  

## Sobre

Este material está sendo elaborado por [kyle Felipe](www.kylefelipe.com) e [Felipe Sodré Barros]() como uma forma de estudar PyQGIS, assim como motivar a outros interessados a seguir a mesma trilha. A principio não se trata de um curso. Talvez esteja mais para um grupo de estudos aberto e colaborativo. Iremos preparando os temas e os processos executados nas lives conforme formos estudando, assim como, quando surjam dúvidas e colaborações. É provável que tenhamos outros colaboradores ao longo desse projeto. Por isso recomendamos consultar [a página de contribuidores - gerada automáticamente pelo git](https://gitlab.com/geocastbrasil/livepyqgis/-/graphs/master) para ver quem mais está envolvido.  

Como esse projeto tem sido desenvolvido por várias pessoas, é provável que algumas aulas reflitam a forma de pensar de um ou outro de nossos colaboradores, o que é ótimo.  
Ex.: Felipe é mais favorável à doutrina "baby steps";
Também conhecido como:  
<img src="https://media.giphy.com/media/3o7TKOuYcKMnqqatjy/giphy.gif"  width="30%" height="30%">  

Já o Kyle é adepto do "fast and furious".  
<img src="http://giphygifs.s3.amazonaws.com/media/bAplZhiLAsNnG/giphy.gif"  width="30%" height="30%">  

Contudo, buscaremos manter um único estilo de programação (*coding style*), que é o [sugerido pelos desenvolvedores do QGIS](https://docs.qgis.org/testing/en/docs/developers_guide/codingstandards.html). Ainda com relação à forma de pensar de cada colaborador, talvez isso faça com que, tenhamos apresentações complementares à anterior. Visando ou repetir o que foi feito antes, mas com uma nova abordagem, ou apenas apresentando uns truques que ficaram de fora, mas que valem a pena serem mencionados.  

A cada live, deixaremos listado neste README os *links* para os principais passos/processos executados, como forma de facilitar a busca de conteúdos. Mas vale mencionar que as lives seguem (ou, assim tentaremos) um começo-meio-fim e, por isso, deve-se considerar que alguns objetos tenham sido criados antes. Por isso, recomendamos precaução.Com relação aos dados usados, tentaremos ao máximo, usar dados abertos e de fácil acesso. Também tentaremos deixar claro os links e, caso necessário, salvaremos alguns dados [numa pasta do Google Drive](https://drive.google.com/drive/folders/1BIb1sCd2G4qPLqrEYapQx-5w_gX5l1lC?usp=sharing).  

Caso tenham dúvidas, sugestões de temas a serem explorados, correções em nossos materiais, nos avise [abrindo uma issue](https://docs.gitlab.com/ee/user/project/issues/) neste repositório, ou vejam [como contribuir](README.md#como-contribuir). Caso queira saber mais sobre git/gitlab, [veja este link](https://medium.com/ekode/primeiros-passos-com-git-e-gitlab-criando-seu-primeiro-projeto-89f9001614b0).  

## Informações técnicas

Iremos usar o QGIS 3 (3.4, até o momento) como base, e a notação do Python 3, que é o python padrão do QGIS3.  

## Breve introdução ao python

Caso você nunca tenha programado com python, considere dar uma olhada antes nessa [breve introdução](IntroPython.md#Breve_introdução_ao_python). Acreditamos ser o mínimo necessário para executar os códigos aqui apresentados. Mas para entender o que está sendo feito, sugerimos que façam um curso (nem que seja básico sobre a linguagem).  

Mas além dessa breve introdução, feita, tivemos algumas lives abordando alguns pontos importantes, comoe strutura de dados. Para facilitar seu acesso listaremos aseguir:  

* Usando outros módulos python: [modulo `os`](estudos/live_1/README.md#organização-de-path);  
* Estrutura de dados: [Listas](estudos/live_2#lista);  
* Estrutura de dados: [Dicionários](estudos/live_3#dicionários);  

## Módulo 1

:warning: vamos usar os dados do [banco de dados do IBGE](https://downloads.ibge.gov.br/downloads_geociencias.htm) 1:1.000.000 ().

Que está dentro de :  
    cartas_e_mapas > bcim > versao2016 > geopackage.

Para fazer [download, clique aqui](http://servicodados.ibge.gov.br/Download/Download.ashx?u=geoftp.ibge.gov.br/cartas_e_mapas/bases_cartograficas_continuas/bcim/versao2016/geopackage/bcim_2016_21_11_2018.gpkg), e guarde-o dentro da pasta "/livepyqgis/dados"  

### Live 1:  ["Quebrando o gelo"](estudos/live_1/README.md#quebrando-o-gelo)  

1. [Abrir uma camada vetorial](estudos/live_1/README.md#Abrir-uma-camada-vatorial);  
1. [Selecionando camada ativa](estudos/live_1/README.md#Selecionando-camada-ativa)  
1. [Contando feições da camada](estudos/live_1/README.md#Contando-feições-da-camada)  
1. [Listando os campos da camada](estudos/live_1/README.md#Listando-os-campos-da-camada)  
1. [Listando os valores de um campo](estudos/live_1/README.md#Listando-os-valores-de-um-campo)  
1. [Filtrar por uma ou mais feições](estudos/live_1/README.md#Filtrar-por-uma-ou-mais-feições)  
1. [Desfazer filtro](estudos/live_1/README.md#Desfazer-filtro)  
1. [Alterando a simbologia](estudos/live_1/README.md#Alterando-a-simbologia)
1. [Salvando o projeto](estudos/live_1/README.md#Salvando-o-projeto)  

### Live 2 (1b e 2):  ["Kyle way of life"](estudos/live_1b/README.md#quebrando-o-gelo)

1. [Abrindo um arquivo GEOPACKAGE](estudos/live_1b/README.md#Abrindo-um-arquivo-geopackage):  
1. [Listando camadas do GEOPACKAGE](estudos/live_1b/README.md#listando-camadas-do-geopackage);  
1. [Adicionando_TODAS_as_camadas_do_GPKG_ao_projeto](estudos/live_1/README.md#Adicionando-todas-as-camadas-do-gpkg-ao-projeto);  
1. [Atribuir a um objeto uma determinada camda com mapLayersByName()](estudos/live_1/README.md#Atribuir-a-um-objeto-uma-determinada-camda-com-mapLayersByName());
1. [Selecionando feições](estudos/live_1b/README.md#Selecionando-feições);  
1. [Estrutura de dados python - Listas](estudos/live_2#lista)  
1. [Trabalhando com rasters](estudos/live_2/Trabalhando-com-RASTERS)  
1. [`QgsProject.instance()`](estudos/live_2#o-que-é-esse-tal-de-qgsprojectinstance)  

### Live 3:  ["Executando algoritmos do processing"](estudos/live_3/README.md)

1. [Introdução sobre dicionários](estudos/live_3/README.md#dicionários)  
1. [Carregando un projeto já existente](estudos/live_3/README.md#carregar-un-projeto-já-existente)  
1. [Propriedades da simbologia ( e dicionários :)](estudos/live_3/README.md#propriedades-da-simbologia-e-dicionários-)  
    1. [Estrutura de simbologia da camada vetorial](estudos/live_3/README.md#estrutura-de-simbologia-da-camada-vetorial)  
1. [Adicionando camada raster](estudos/live_3/README.md#adicionando-camada-raster)  
1. [Atribuir a uma variável uma camada com `mapLayersByName()`](estudos/live_3/README.md#atribuir-a-uma-variável-uma-camada-com-maplayersbyname)  
1. [Executando algoritmos do Processing](estudos/live_3/README.md#executando-algoritmos-do-processing)  
    1. [Projetando uma camada vetorial](estudos/live_3/README.md#executando-nosso-primeiro-algoritmo-qgisreprojectlayer)  
    1. [Hillshade: Carregando após processamento (`processing.runAndLoadResults`)](estudos/live_3/README.md#hillshade-carregando-após-processamento-processingrunandloadresults)  
    1. [Slope (`gdal:slope`)](estudos/live_3/README.md#slope-gdalslope)  

### Live 4: ["Catalog on the fly paraguaio"](estudos/live_4/README.md)

1. [Preparação do projeto](estudos/live_4#preparando-o-projeto);  
1. [Criando uma ação com python](estudos/live_4#qgis-python-ação);  

### Live 5: ["Criando um script *standalone*"](estudos/live_5/README.md)

1. [O que é um Script *standalone*](estudos/live_5#O-que-é-um-script-standalone);  
1. Proposta da live: [Desenvolver ferramenta de dados legado](estudos/live_5#Desenvolvendo-ferramenta-de-dados-legado);  
1. [Criando a ferramenta no QGIS](estudos/live_5#Criando-a-ferramenta-no-QGIS);
1. [Transformando em script standalone](estudos/live_5#Transformando-em-script-standalone);  

### Live 6: ["Geometry Park"](estudos/live_6/README.md)

1. [Compondo a Geometria - Teoria](estudos/live_6/README.md#compondo-a-geometria-teoria)
1. [E no código? Como é que fica?](estudos/live_6/README.md#e-no-código-como-é-que-fica)
1. [Direto ao Ponto](estudos/live_6/README.md#direto-ao-ponto)
1. [Montando a Linha](estudos/live_6/README.md#montando-a-linha)
1. [Montando o Poligono](estudos/live_6/README.md#montando-poligono)

### Live 7: [Criando Atributos](estudos/live_7/README.md)

1. [Criando Atributos](estudos/live_7/README.md#criandos-atributos)
1. [Os Campos da Tabela](estudos/live_7/README.md#os-campos-da-tabela)
1. [Criando Campos](estudos/live_7/README.md#criando-campos)
1. [Populando os Campos](estudos/live_7/README.md#populando-os-campos)

### Live 8: [Manipulação de dados raster](estudos/live_8/README.md)

1. [Carregando a imagem ao projeto](estudos/live_8/README.md#carregando-a-imagem-ao-projeto)
1. [Tipo de camada](estudos/live_8/README.md#tipo-de-camada)
1. [Quantidade de bandas da imagem](estudos/live_8/README.md#quantidade-de-bandas-da-imagem)
1. [Sobre renderização do raster](estudos/live_8/README.md#sobre-renderização-do-raster)
1. [Ajustando contraste da imagem](estudos/live_8/README.md#ajustando-contaste-da-imagem)
1. [Alterando a composição de visualização](estudos/live_8/README.md#alterando-a-composição-de-visualização)
1. [Raster calculator (*gdal:rastercalculator*)](estudos/live_8/README.md#raster-calculator)
1. [Alterando visualização](estudos/live_8/README.md#alterando-visualização)
1. [Calculando estatísticas](estudos/live_8/README.md#calculando-estatisticas)
1. [Definido uma rampa de cor (na unha, com QgsColorRampShader)](estudos/live_8/README.md#definido-uma-rampa-de-cor-na-unha-com-qgscolorrampshader)
1. [Gerando Histograma de MultiBandRasterLayer](estudos/live_8/README.md#gerando-histograma-de-multibandrasterlayer)
1. [Histograma de SingleBandRasterLayer](estudos/live_8/README.md#histograma-de-singlebandrasterlayer)

### Live 9: Rodando script em "background" no QGIS

1. [O que é background](estudos/live_8/README.md#o-que-é-background)
1. [Uso da QgsTask.fromFunction](estudos/live_8/README.md#uso-da-qgstaskfromfunction)
1. [Cria uma QgsTask](estudos/live_8/README.md#)
1. [Adiciona ao gerênciador de tarefas e executa](estudos/live_8/README.md#adicina-ao-gerênciador-de-tarefas-e-executa)

## Mais conteúdos

* [PyQGIS 101: Introduction to QGIS Python programming for non-programmers](https://anitagraser.com/pyqgis-101-introduction-to-qgis-python-programming-for-non-programmers/);  
* [PyQGIS Developer Cookbook](https://docs.qgis.org/testing/en/docs/pyqgis_developer_cookbook/);  
* [The PyQGIS Programmer's Guide](https://locatepress.com/ppg3);  
* [PyQGIS Documentation](https://qgis.org/api/api_break.html#qgis_api_break_3_0_QgsLayerTreeMapCanvasBridge);  
* [PyQGIS Blog](https://pyqgis.blogspot.com/);  
* [PyQGIS SheatSheet](https://docs.qgis.org/testing/en/docs/pyqgis_developer_cookbook/cheat_sheet.html);  
* [Repositório do curso Python e QGIS](https://github.com/volaya/qgis-python-course) do [Victor Olaya](https://twitter.com/volayaf);  
* [QGIS API annotated](https://qgis.org/api/annotated.html)  
* [Amazeone](http://amazeone.com.br/)

## Venha trocar umas ideias

[YouTube](https://www.youtube.com/channel/UCLAeX4dyujMoy4xqHvxSDpQ)  
[Telegram](https://t.me/thinkfreeqgis)  
[Whatsapp](https://chat.whatsapp.com/Iklk4rrbq8k16IEiRfaEMB)  
[Instagram](https://www.instagram.com/geocastbrasil)

## Como contribuir

Como boas práticas, faça o *fork* desse repositório, faça as modificações sugeridas e abrar um *pull request*;  
